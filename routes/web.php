<?php

use App\Http\Controllers\AgentController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/agent/admin', [HomeController::class, 'adminIndex'])->name('adminhome');
Route::get('/agent/add', [AgentController::class, 'index'])->name('agentadd');
Route::get('/agent/view', [AgentController::class, 'showAll'])->name('agentview');

Route::get('/admin/{id}', [AgentController::class, 'showSingleAgent'])->name('singleagent');

Route::post('/agents', [AgentController::class, 'store']);
